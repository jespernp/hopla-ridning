jQuery(document).ready(function() {
    jQuery(window).scroll(function() {
        fixedElement();
    });
    jQuery('.responsive-menu').click(function() {
        jQuery('html').toggleClass("menu-open");
    });
    jQuery('#close-icon').click(function() {
        jQuery('html').toggleClass("menu-open");
    });
});


function fixedElement() {
    console.log("Scroll");
    var fixedElement = jQuery('nav');
    var windowPosition = jQuery(window).scrollTop();
    var elementPosition = jQuery("header").offset().top;

    if (windowPosition > 170) {
        fixedElement.addClass("fixed");
    } else {
        fixedElement.removeClass("fixed");
    }
};